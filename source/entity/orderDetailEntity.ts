import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { OrderEntity } from "./orderEntity";
import { ProductEntity } from "./productEntity";

@Entity("order_detail")
export class OrderDetailEntity extends BaseEntity {
    @Column("uuid", {
        primary: true,
        name: "id",
        default: () => "uuid_generate_v4()",
    })
    @PrimaryGeneratedColumn()
    id!: string;

    @Column("uuid", { name: "order_id" })
    orderId!: string;

    @Column("uuid", { name: "product_id" })
    productId!: string;

    @Column("int", { name: "qty" })
    qty!: number;

    @Column("numeric", { name: "price" })
    price!: number;

    @Column("numeric", { name: "amount" })
    amount!: number;

    @ManyToOne(() => OrderEntity, orderEntity => orderEntity.orderDetail)
    @JoinColumn([{ name: "order_id", referencedColumnName: "id" }])
    order!: OrderEntity;

    @OneToOne(() => ProductEntity, productEntity => productEntity.orderDetail)
    @JoinColumn([{ name: "product_id", referencedColumnName: "id" }])
    product!: ProductEntity;
}