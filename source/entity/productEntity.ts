import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany, OneToOne } from "typeorm";
import { OrderDetailEntity } from "./orderDetailEntity";

@Entity("product")
export class ProductEntity  extends BaseEntity {
    @Column("uuid", {
        primary: true,
        name: "id",
        default: () => "uuid_generate_v4()",
    })
    @PrimaryGeneratedColumn()
    id!: string;

    @Column("character varying", { name: "name", length: 100 })
    name!: string;

    @Column("numeric", { name: "price" })
    price!: number;

    @Column("int", { name: "qty" })
    qty!: number;
    
    @OneToOne(() => OrderDetailEntity, orderDetailEntity => orderDetailEntity.product)
    orderDetail!: OrderDetailEntity;
}