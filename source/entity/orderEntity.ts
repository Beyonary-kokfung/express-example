import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, OneToMany } from "typeorm";
import { OrderDetailEntity } from "./orderDetailEntity";

@Entity("order")
export class OrderEntity extends BaseEntity {
    @Column("uuid", {
        primary: true,
        name: "id",
        default: () => "uuid_generate_v4()",
    })
    @PrimaryGeneratedColumn()
    id!: string;
    
    @Column("character varying", { name: "name", length: 255 })
    name!: string;

    @CreateDateColumn({ type: 'timestamptz', name: 'date' })
    date!: Date;

    @OneToMany(() => OrderDetailEntity, orderDetailEntity => orderDetailEntity.order)
    orderDetail!: OrderDetailEntity[];
}