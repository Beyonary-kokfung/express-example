import { Response } from 'express';
import { getManager } from 'typeorm';
import { OrderDetailEntity } from '../entity/orderDetailEntity';
import { OrderEntity } from '../entity/orderEntity';
import { ProductEntity } from '../entity/productEntity';
import { OrderModel } from '../model/orderModel';
import { CustomRequest } from '../utils/CustomRequest';

const list = async (req: Request, res: Response) => {
    try {
        let list = await OrderEntity.createQueryBuilder("order")
            .innerJoinAndSelect("order.orderDetail", "orderDetail")
            .innerJoinAndSelect("orderDetail.product", "product")
            .orderBy("order.date","DESC")
            .getMany();
        return res.status(200).json(list);
    } catch (error) {

        res.status(500).send(error.message);
    }

};

const save = async (req: CustomRequest<OrderModel>, res: Response) => {
    try {
        let body = req.body;

        if (!body.name) res.status(400).send("Name is required");
        if (!body.orderList || body.orderList?.length == 0) res.status(400).send("At least one order");
        await getManager().transaction(async (manager) => {
            let taskList: Promise<void>[] = [];
            let order = new OrderEntity();
            order.name = body.name;
            await manager.getRepository(OrderEntity).insert(order);

            body.orderList.forEach(x => {
                let task = new Promise<void>(async (resolve) => {
                    let product = await manager.getRepository(ProductEntity).findOne(x.productId, {
                        lock: {
                            mode: "pessimistic_write"
                        }
                    });
                    if (!product) {
                        return res.status(400).send("Product not exist");
                    }
                    let qty = product.qty - x.qty;
                    if (qty < 0) return res.status(400).send(`${product.name} not enough stock`);

                    await manager.getRepository(OrderDetailEntity).insert({
                        orderId: order.id,
                        productId: product.id,
                        price: product.price,
                        qty: x.qty,
                        amount: product.price * x.qty
                    });
                    await manager.getRepository(ProductEntity).update(product.id, {
                        qty: qty
                    });

                    resolve();
                });

                taskList.push(task);
            });

            await Promise.all(taskList);

        });


        res.status(201).send();
    } catch (error) {

        res.status(500).send(error.message);
    }

};

export default { save, list };
