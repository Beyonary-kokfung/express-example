import {  Request, Response } from 'express';
import { ProductEntity } from '../entity/productEntity';

const productList = async (req: Request, res: Response) => {
    try {
        let list = await ProductEntity.find({
            order: {
                name: "ASC"
            }
        });
        return res.status(200).json(list);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

export default { productList };
