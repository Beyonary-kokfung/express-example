import express from 'express';
import controller from '../controllers/order';

const router = express.Router();

router.get('/', controller.list as any);
router.post('/save', controller.save);

export = router;
