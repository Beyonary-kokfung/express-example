export interface OrderModel{
    name:string;
    orderList:OrderDetailModel[]
}
export interface OrderDetailModel{
    productId:string;
    qty:number;
}