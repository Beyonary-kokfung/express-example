import "reflect-metadata";
import http from 'http';
import bodyParser from 'body-parser';
import express from 'express';
import config from './config/config';
import productRoutes from './routes/product';
import orderRoutes from './routes/order';
import { createConnection } from "typeorm";
import { ProductEntity } from "./entity/productEntity";
import { OrderEntity } from "./entity/orderEntity";
import { OrderDetailEntity } from "./entity/orderDetailEntity";

const NAMESPACE = 'Server';
const router = express();


const server = async () => {
    const conn = await createConnection({
        host: "satao.db.elephantsql.com",
        password: "sqKyLGlTEfdSDqvL65Gd4tTRAchjnsgM",
        type: "postgres",
        username: "koshdclg",
        database: "koshdclg",
        port: 5432,
        logging: true,
        synchronize: false,
        entities: [ProductEntity,OrderEntity,OrderDetailEntity],
    });
    console.log("Database connected :", conn.options.database);
}


/** Log the request */
router.use((req, res, next) => {
    /** Log the req */
    console.log(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

    res.on('finish', () => {
        /** Log the res */
        console.log(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
    })

    next();
});

/** Parse the body of the request */
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

/** Rules of our API */
router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});

/** Routes go here */
router.use('/api/product', productRoutes);
router.use('/api/order', orderRoutes);

/** Error handling */
router.use((req, res, next) => {
    const error = new Error('Not found');

    res.status(404).json({
        message: error.message
    });
});

const httpServer = http.createServer(router);

httpServer.listen(config.server.port, () => console.log(NAMESPACE, `Server is running on port ${config.server.port}`));
server().catch((err) => console.trace(err));